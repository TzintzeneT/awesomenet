# AwesomeNet

Java package containing a simple server and a simple client, meant to help you create other network-based utilitys.

## functionality:
- Listening for messages: `listen(), stopListening(), isListening()`
- Sending messages: `sendMessage(), sendMessage(String message)`

## example usecase
you can see the source-code for the [LokTalk utility](https://gitlab.com/TzintzeneT/loctalk) for example usage.

## Thanks
[That](https://www.youtube.com/playlist?list=PLoW9ZoLJX39Xcdaa4Dn5WLREHblolbji4) playlist from the youtube channel [Abhay Redkar](https://www.youtube.com/@abhayredkar4051) is awesome, a greate resorce, and I cannot recomend it enugh.

## License
Using the awesome GPLv3

## Created using FOSS
Because FOSS is awesome
