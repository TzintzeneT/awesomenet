package AwesomeNet;
public abstract class based{
  public abstract void sendMessage();

  public abstract void sendMessage(String message);

  public abstract void listen();

  public abstract void stopListening();

  public abstract boolean isListening();
}
