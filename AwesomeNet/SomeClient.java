package AwesomeNet;

import java.net.Socket;
import java.io.*;

public class SomeClient extends based {

    private final int serverPort;
    private Socket theClient;
    private BufferedReader input;
    private PrintWriter output;
    private boolean listening = false;

    public SomeClient(int serverPort) {
        this.serverPort = serverPort;
    }

    public void startClient() {
        try {
            this.theClient = new Socket("localhost", this.serverPort);
            this.input = new BufferedReader(new InputStreamReader(theClient.getInputStream()));
            this.output = new PrintWriter(theClient.getOutputStream(), true);
            System.out.println("connected (as client)");
        } catch (Exception e) {
            System.out.println("client crashed");
            System.out.println(e);
        }
    }

    public void listen() {
        try {
            this.listening = true;
            String message = input.readLine();

            while (message != null && this.listening) {
                System.out.println(this.serverPort + ": " + message);
                message = input.readLine();
            }

            System.out.println("server disconnected");
            this.stopListening();

        } catch (Exception e) {
            System.out.println("client crashed");
            System.out.println(e);
            this.stopListening();
        }
    }

    public void stopListening() {
        this.listening = false;
    }

    public void sendMessage(String message) {
        try {
            //System.out.println("You (client): " + message);
            output.println(message);
        } catch (Exception e) {
            System.out.println("Error: cannot send message");
            System.out.println(e);
        }
    }

    public boolean isListening() {
        return this.listening;
    }

    public void sendMessage() {
        sendMessage("Hello, world!");
    }
}
