package AwesomeNet;

import java.net.*;
import java.io.*;

public class AwesomeServer extends based {

    private final int serverPort;
    private ServerSocket server;
    private Socket theClient;
    private BufferedReader input;
    private PrintWriter output;
    private boolean listening = false;

    public AwesomeServer(int serverPort) {
        this.serverPort = serverPort;
    }

    public void startServer() {
        try {
            this.server = new ServerSocket(this.serverPort);
            System.out.println("waiting...");
            this.theClient = server.accept();
            this.input = new BufferedReader(new InputStreamReader(theClient.getInputStream()));
            this.output = new PrintWriter(theClient.getOutputStream(), true);
            System.out.println("connected (as server)");
        } catch (Exception e) {
            System.out.println(e);
            throw new RuntimeException("cannot open a server");
        }
    }

    public void listen() {

        try {
            this.listening = true;
            String message = input.readLine();

            while (message != null && this.listening) {
                System.out.println(this.serverPort + ": " + message);
                message = input.readLine();
            }

            System.out.println("client disconnected");
            this.stopListening();

        } catch (Exception e) {
            System.out.println("Server crashed");
            System.out.println(e);
            this.stopListening();
        }
    }

    public void stopListening() {
        this.listening = false;
    }

    public boolean isListening() {
        return this.listening;
    }

    public void sendMessage(String message) {
        try {
            output.println(message);
        } catch (Exception e) {
            System.out.println("Error: cannot send message");
            System.out.println(e);
        }
    }

    public void sendMessage() {
        sendMessage("Hello, world!");
    }
}
